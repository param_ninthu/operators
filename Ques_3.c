#include <stdio.h>
/* Question No 3 */
int main()
{
    int num_1 , num_2 ;
    printf("Enter the First Number : ");
    scanf("%d",&num_1);
    printf("Enter the Second Number : ");
    scanf("%d",&num_2);
    printf("Before swap \nnum_1=%d \nnum_2=%d",num_1 , num_2);
    num_1 += num_2;
    num_2 = num_1 - num_2;
    num_1 -= num_2 ;
    printf("\nAfter swap \nnum_1=%d \nnum_2=%d",num_1 , num_2);

    return 0;
}
