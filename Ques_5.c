#include <stdio.h>
/* Question No 5 */
int main()
{
    int A=10 ,B =15 ;
    int Bitwise_AND , Bitwise_XOR , Bitwise_NOT ,Bitwise_Leftshift , Bitwise_Rightshift ;
    Bitwise_AND = A & B ;
    Bitwise_XOR = A ^ B ;
    Bitwise_NOT = ~A;
    Bitwise_Leftshift = A << 3;
    Bitwise_Rightshift = B >> 3;
    
    printf("The value of Bitwise_AND =%d\n",Bitwise_AND);
    printf("The value of Bitwise_XOR =%d\n",Bitwise_XOR);
    printf("The value of Bitwise_NOT =%d\n",Bitwise_NOT);
    printf("The value of Bitwise_Leftshift =%d\n",Bitwise_Leftshift);
    printf("The value of Bitwise_Rightshift =%d",Bitwise_Rightshift);
}
