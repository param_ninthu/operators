#include <stdio.h>
#include <math.h>
/* Question No 2 Finding volume of the Cone */
int main()
{
    float Volume,height,radius, pi=22/7.0;
    printf("Calculate the Volume of the Cone \n");
    printf("Enter the Height of the cone : ");
    scanf("%f",&height);
    printf("Enter the Radius of the cone : ");
    scanf("%f",&radius);
    Volume = pi *pow(radius,2) * height/3.0;
    printf("Volume of the Cone is :%f",Volume);
    return 0;
}

